import 'package:flutter/widgets.dart';

// Sizes text to fit a percentage of the device width as an extension method:
// usage: 'Hello world!'.scaledText(0.1)
extension TextSizeExtension on String {
  Widget scaledText(double scale, {TextStyle? style}) {
    return LayoutBuilder(builder: (context, constraints) {
      final width = constraints.maxWidth;
      final fontSize = width * scale;

      return Text(
        this,
        style: style?.copyWith(fontSize: fontSize),
      );
    });
  }
}
