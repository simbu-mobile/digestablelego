import 'package:flutter/widgets.dart';

/// Calculates the font size as a percentage of the screen width.
extension TextExtension on BuildContext {
  double fontSize(double percentage) {
    final MediaQueryData data = MediaQuery.of(this);
    final double screenWidth = data.size.width;
    return screenWidth * percentage / 100;
  }
}