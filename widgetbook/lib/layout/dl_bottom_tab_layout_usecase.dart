// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:layout_lego/dl_bottom_tab_layout.dart';
import 'package:layout_lego/models/dl_tab_item.dart';
import 'package:simbucore/app/core/model/platform_type.dart';

// Package imports:
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

List<DlTabItem> get exampleIosTabItems => <DlTabItem>[
      DlTabItem(
        label: "Digests",
        icon: const Icon(CupertinoIcons.bars),
        content: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Text("How many programmers does it take to change a light bulb ?"),
              Text(""),
              Text("None, its a hardware problem"),
            ],
          ),
        ),
        globalKey: GlobalKey<NavigatorState>(),
        navigationBar: null,
      ),
      DlTabItem(
        label: "Friends",
        icon: const Icon(CupertinoIcons.group),
        content: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Text("Two bytes meet. The first byte asks, 'Are you ill?'"),
              Text(""),
              Text("The second byte replies, 'No, just feeling a bit off.'"),
            ],
          ),
        ),
        globalKey: GlobalKey<NavigatorState>(),
        navigationBar: null,
      ),
      DlTabItem(
        label: "History",
        icon: const Icon(CupertinoIcons.time),
        content: const Center(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                  "There’s a band called 1023MB. They haven’t had any gigs yet."),
            )),
        globalKey: GlobalKey<NavigatorState>(),
        navigationBar: null,
      ),
    ];

@WidgetbookUseCase(name: 'iOS', type: DlBottomTabLayout)
Widget bottomTabLayout(BuildContext context) {
  return DlBottomTabLayout(PlatformType.iOS, exampleIosTabItems);
}
