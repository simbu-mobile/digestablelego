// Flutter imports:
import 'package:asset_lego/dl_avatar.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

Widget build(Widget child) {
  return CupertinoPageScaffold(
    navigationBar: const CupertinoNavigationBar(
      middle: Text("Circular Avatar"),
    ),
    child: Center(child: child),
  );
}

//title: Text(context.knobs.text(label: 'Title', initialValue: "Title"))
@WidgetbookUseCase(name: 'Title Only', type: DlAvatar)
Widget circularAvatar(BuildContext context) {
  return build(
    const DlAvatar(
      Image(
        image: AssetImage("assets/imgs/flutter.png"),
      ),
    ),
  );
}
