// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// WidgetbookGenerator
// **************************************************************************

import 'dart:convert';
import 'dart:core';
import 'dart:typed_data';
import 'package:core_widgetbook/animation/dl_lottie_usecase.dart';
import 'package:core_widgetbook/layout/dl_bottom_tab_layout_usecase.dart';
import 'package:core_widgetbook/layout/dl_list_usecase.dart';
import 'package:core_widgetbook/asset/dl_avatar_usecase.dart';
import 'package:core_widgetbook/app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:widgetbook/widgetbook.dart';

void main() {
  runApp(HotReload());
}

class HotReload extends StatelessWidget {
  const HotReload({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Widgetbook.material(
      appInfo: AppInfo(
        name: 'Widgets',
      ),
      themes: [
        WidgetbookTheme(
          name: 'Light',
          data: lightTheme(),
        ),
        WidgetbookTheme(
          name: 'Dark',
          data: darkTheme(),
        ),
      ],
      devices: [
        Device(
          name: 'iPhone 12',
          resolution: Resolution(
            nativeSize: DeviceSize(
              height: 2532.0,
              width: 1170.0,
            ),
            scaleFactor: 3.0,
          ),
          type: DeviceType.mobile,
        ),
        Device(
          name: 'iPhone 13',
          resolution: Resolution(
            nativeSize: DeviceSize(
              height: 2532.0,
              width: 1170.0,
            ),
            scaleFactor: 3.0,
          ),
          type: DeviceType.mobile,
        ),
        Device(
          name: '7.9" iPad mini',
          resolution: Resolution(
            nativeSize: DeviceSize(
              height: 1024.0,
              width: 768.0,
            ),
            scaleFactor: 2.0,
          ),
          type: DeviceType.tablet,
        ),
        Device(
          name: 'Desktop 1080p',
          resolution: Resolution(
            nativeSize: DeviceSize(
              height: 1080.0,
              width: 1920.0,
            ),
            scaleFactor: 2.0,
          ),
          type: DeviceType.desktop,
        ),
      ],
      textScaleFactors: [
        1.0,
        1.5,
        2.0,
      ],
      categories: [
        WidgetbookCategory(
          name: 'Animation',
          folders: [
            WidgetbookFolder(
              name: 'Providers',
              widgets: [
                WidgetbookComponent(
                  name: 'Lottie Json Loader',
                  useCases: [
                    WidgetbookUseCase(
                      name: 'Display Animation',
                      builder: (context) => lottie(context),
                    ),
                  ],
                ),
              ],
              folders: [],
            ),
          ],
          widgets: [],
        ),
        WidgetbookCategory(
          name: 'Assets',
          folders: [
            WidgetbookFolder(
              name: 'Images',
              widgets: [
                WidgetbookComponent(
                  name: 'Avatar',
                  useCases: [
                    WidgetbookUseCase(
                      name: 'Circular',
                      builder: (context) => circularAvatar(context),
                    ),
                  ],
                ),
              ],
              folders: [],
            ),
          ],
          widgets: [],
        ),
        WidgetbookCategory(
          name: 'Layout',
          folders: [
            WidgetbookFolder(
              name: 'Navigation',
              widgets: [
                WidgetbookComponent(
                  name: 'Tabbed Layout',
                  useCases: [
                    WidgetbookUseCase(
                      name: 'Bottom',
                      builder: (context) => bottomTabLayout(context),
                    ),
                  ],
                ),
              ],
              folders: [],
            ),
            WidgetbookFolder(
              name: 'Containers',
              widgets: [
                WidgetbookComponent(
                  name: 'List',
                  useCases: [
                    WidgetbookUseCase(
                      name: 'Title Only',
                      builder: (context) => dlListTitleOnly(context),
                    ),
                    WidgetbookUseCase(
                      name: 'Title and Subtitle',
                      builder: (context) => dlListTitleAndSubtitle(context),
                    ),
                    WidgetbookUseCase(
                      name: 'Avatar, Title and Subtitle',
                      builder: (context) => dlListAvatarTitleAndSubtitle(context),
                    ),
                    WidgetbookUseCase(
                      name: 'Items',
                      builder: (context) => dlListExample(context),
                    ),
                    
                  ],
                ),
              ],
              folders: [],
            ),
          ],
          widgets: [],
        ),
      ],
    );
  }
}
