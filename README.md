# Flutter Components

Flutter components is a library that comprises of: 

* A [Widgetbook](https://www.widgetbook.io) used to display the components 
* A series of packages that contains the widgets and tests 
* Some utilities to help with testing and other tasks.

![Mono Repo Dirs](doco\mono_repo_dirs.png)

It is a mono repo with packages instead of a multi-repo, the advantage is one project to develop in, but application can still choose which packages they want to include, feels like the sweet spot.

![Mono Repo Design](doco\mono_repo_design.png)

## Adding New Widgets

Each new widget needs three new files:

* Widget:  Added to the package project.
* Widget Test:  Added to the package project.
* Widget Usecase: Added to the Widgetbook project.

E.G. For the Lottie file loader:

* /packages/animation/lib/providers/dl_lottie_json_loader.dart (Widget)
* /packages/animation/test/providers/dl_lottie_json_loader_test.dart (Widget Test)
* /widgetbook/lib/providers/dl_lottie_json_loader_usecase.dart (Widget Usecase)

## Adding a new package

Create a package under top level packages dir:
``` bash
flutter create --template=package <PackageName>
```


Hook it up in the widgetbook/ pubspec.yaml file:
``` yaml
dependencies:
    animation:
        path: ../packages/animation
```

## Including a package in an application

Applications can then include the any or all of the packages by adding dependencies in their pubspec.yaml file:

![Mono Repo Package Ref](doco\ref_package.png)

# Road Map

## Add Components to support a cross platform Native Experience

[Google/Flutter Widgets](https://docs.flutter.dev/development/ui/widgets)

Especially Material and Cupertino for creating native experiences on mobile and the web.

[Android & iOS guidelines](https://www.learnui.design/blog/ios-vs-android-app-ui-design-complete-guide.html)

Trial direction is to have widgets that know the device and provide the correct native experience e.g.
DlLayout (Would select the appropraite child widget (Abstraction))
    DlLayoutCupertino - on iOS moible devices will have a bottom tabbed layout.
    DlLayoutAndroid - on Andriod mobile devices will have a side bar.
    DlLayoutWeb - Web TBA, sidebar or trad menu.
    
# Additional Packages

* Accessiblity - Make the app accessible.
* Motion - Scrolling (Parallax) and effects.
* Painting - These widgets apply visual effects to the children without changing their layout, size, or position.
* Stylin - Branding and appearance, typography, logo's etc...

https://docs.flutter.dev/development/ui/widgets
https://medium.com/@simbu/flutter-packages-3b0beea520e0

# Themes

Addition of themes to support branding

# Knobs

Consider using options knob for avatar to switch out images

E.G.. 
Icon(widget.iconData ?? Icons.add)

iconData: context.knobs.options(
                      label: 'Icon',
                      options: [
                        Icons.add,
                        Icons.crop_square_sharp,
                        Icons.circle
                      ],
                    ),
 */

# Known Issues

* .withMaterialApp() extn not working

# Gothas

* Failing asset tests - Unable to load asset: "packages/assets/images/person.png"
    pubspec.yaml
    flutter:
    [2 whitespaces or 1 tab]assets:
    [4 whitespaces or 2 tabs]- images/  #index all files inside images folder
    code
    AssetImage('images/person.png') 
    Note, that the AssetImage path starts from the top level of assets declared in the pubspec.yaml not the project
    When u add to pubspec it then builds the asset resources using the paths u specify and so 
    in this example its 'images/...' not 'packages/assets/images/...' or '../images/...'
