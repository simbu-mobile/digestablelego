// Flutter imports:
import 'package:flutter/material.dart';

/// Circle avatar with border.
class DlAvatar extends StatelessWidget {
  final Image image;
  final double? radius;
  const DlAvatar(this.image, {Key? key, this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var avatarRadius = radius ?? 36;
    return CircleAvatar(
      backgroundColor: Colors.black54,
      radius: avatarRadius,
      child: CircleAvatar(
        backgroundImage: image.image,
        radius: avatarRadius - 1,
      ),
    );
  }
}