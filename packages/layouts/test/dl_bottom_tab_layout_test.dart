/*
  BottomTabLayout - Well polished useful widget, brilliantly coded with care.
*/
// #     #                                 #######
// #  #  # # #####   ####  ###### #####       #    ######  ####  #####
// #  #  # # #    # #    # #        #         #    #      #        #
// #  #  # # #    # #      #####    #         #    #####   ####    #
// #  #  # # #    # #  ### #        #         #    #           #   #
// #  #  # # #    # #    # #        #         #    #      #    #   #
//  ## ##  # #####   ####  ######   #         #    ######  ####    #

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:layout_lego/dl_bottom_tab_layout.dart';
import 'package:layout_lego/models/dl_tab_item.dart';

// Project imports:
import 'package:simbucore/app/core/model/platform_type.dart';
import 'package:utils/fixture/finders.dart';
import 'package:utils/fixture/widget_extension.dart';
import 'fixture/tab_items.dart';

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

Widget buildWidget(PlatformType platformType, {List<DlTabItem>? tabItems}) {

  tabItems ??= exampleIosTabItems;

  if (platformType == PlatformType.android) {
    return DlBottomTabLayout(platformType, tabItems)
        .withMaterialApp();
  }

  return  DlBottomTabLayout(platformType, tabItems)
      .withCupertinoApp();
}

void main() {
  group('BottomTabLayout', () {
    cupertinoTabbedLayoutForIosDevices();
    materialTabbedLayoutForAndroidDevices();
    errorsWhenNoTabItems();
    errorsWhenOneTabItem();
    errorsWhenSixTabItems();
  });

  group('Tabbed Layout, Content', () {
    hasContentArea();
    canChangeContent();
  });

  group('Tabbed Layout, Tab Bar', () {
    hasTabBar();
    tabItemsArePassedIn();
  });

  group('Tabbed Layout, Tab Item', () {
    displaysName();
    displaysIcon();
  });
}

Future<void> cupertinoTabbedLayoutForIosDevices() async {
  return testWidgets('Uses a Cupertino Tabbed Layout for iOS devices',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS));
    isPresent<IosTabbedLayout>();
  });
  //BottomNavigationBar
}

Future<void> materialTabbedLayoutForAndroidDevices() async {
  return testWidgets('Uses a Material Tabbed Layout for Android devices',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.android));
    isPresent<BottomNavigationBar>();
  });
}

Future<void> errorsWhenNoTabItems() async {
  return testWidgets('Errors when no tab items added',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: []));
    expect(tester.takeException(), isInstanceOf<AssertionError>());
  });
}

Future<void> errorsWhenOneTabItem() async {
  return testWidgets('Errors when one tab item', (WidgetTester tester) async {
    await tester
        .pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(1)));
    expect(tester.takeException(), isInstanceOf<AssertionError>());
  });
}

Future<void> errorsWhenSixTabItems() async {
  return testWidgets('Errors when more than 5 tab items added',
      (WidgetTester tester) async {
    await tester
        .pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(6)));
    expect(tester.takeException(), isInstanceOf<AssertionError>());
  });
}

Future<void> hasContentArea() async {
  return testWidgets('Has a Content Area', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(2)));
    expect(find.byKey(const Key("TabbedLayoutContentArea0")), findsOneWidget);
  });
}

Future<void> canChangeContent() async {
  return testWidgets('Changes Content Area when a tab is pressed', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(2)));
    await tester.tap(find.text('label1'));
    await tester.pump();
    expect(find.byKey(const Key("TabbedLayoutContentArea1")), findsOneWidget);
  });
}

Future<void> hasTabBar() async {
  return testWidgets('Has a Tab Bar', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(2)));
    expect(find.byKey(const Key("TabbedLayoutTabBar")), findsOneWidget);
  });
}

Future<void> tabItemsArePassedIn() async {
  return testWidgets('The Tab Items are passed in', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(2)));
    expect(find.byIcon(CupertinoIcons.group), findsWidgets);
  });
}

Future<void> displaysName() async {
  return testWidgets('Displays the tab name', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(3)));
    expect(find.text("label1"), findsOneWidget);
  });
}

Future<void> displaysIcon() async {
  return testWidgets('Displays the tab icon', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(PlatformType.iOS, tabItems: buildTabItems(3)));
    expect(find.byIcon(CupertinoIcons.group),
        findsNWidgets(3));
  });
}
