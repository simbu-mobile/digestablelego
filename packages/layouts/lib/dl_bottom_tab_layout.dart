/*
  Cross platform Bottom Tab Layout, switches out the bottom tab layout dependent on the device platform e.g. Ios, Android...
*/

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:layout_lego/models/dl_tab_item.dart';
import 'package:simbucore/app/core/model/platform_type.dart';

// Project imports:
//import 'package:simbucore/app/core/model/platform_type.dart';


class DlBottomTabLayout extends StatelessWidget {
  final PlatformType platformType;
  final List<DlTabItem> tabItems;

  const DlBottomTabLayout(this.platformType, this.tabItems, {Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    assert(tabItems.length > 1);
    assert(tabItems.length < 6);

    if (platformType == PlatformType.iOS) {
      return IosTabbedLayout(tabItems);
    }

    return AndroidTabbedLayout(tabItems);
  }
}

/// iOS Bottom Tab Layout.
class IosTabbedLayout extends StatelessWidget {
  final List<DlTabItem> tabItems;
  const IosTabbedLayout(this.tabItems, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var navBarItems = List.generate(tabItems.length, (int index) {
      return tabItems.buildBottomNavBarItem(index);
    });
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        key: const Key("TabbedLayoutTabBar"),
        items: navBarItems,
      ),
      tabBuilder: (context, index) {
        return CupertinoTabView(
          navigatorKey: tabItems[index].globalKey,
          builder: (BuildContext context) => tabItems[index].content,
        );
      },
    );
  }
}

/// Android bottom tab layout.
class AndroidTabbedLayout extends StatelessWidget {
  final List<DlTabItem> tabItems;
  const AndroidTabbedLayout(this.tabItems, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var navBarItems = List.generate(tabItems.length, (int index) {
      return tabItems.buildBottomNavBarItem(index);
    });

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(items: navBarItems),
    );
  }
}