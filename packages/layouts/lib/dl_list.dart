// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Project imports:
import 'package:layout_lego/dl_list_tile.dart';
import 'package:layout_lego/models/dl_list_item.dart';

/// Scrollable list.
class DlList extends StatelessWidget {
  final List<DlListItem> listItems;
  final EdgeInsetsGeometry? margin;
  final Border? tileBorder;
  const DlList(this.listItems, {Key? key, this.margin, this.tileBorder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var listView = ListView.builder(
        itemCount: listItems.length,
        itemBuilder: (context, index) {
          return DlListTile(
            listItems[index],
            tileBorder: tileBorder,
          );
        });

    return Container(
      margin: margin ?? const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: listView,
    );
  }
}
