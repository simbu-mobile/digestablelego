// Dart imports:
import 'dart:convert';
import 'dart:typed_data';

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:lottie/lottie.dart';

class DlLottieJsonLoader extends StatefulWidget {

  static const String assertionFailureRequiresAssetFileOrJsonString = "Please provide an 'assetPath' to a lottie Json file or the Json 'lottieJson'.";

  final String? assetPath;
  final String? lottieJson;

  const DlLottieJsonLoader({Key? key, this.assetPath, this.lottieJson}) : super(key: key);

  @override
  State<DlLottieJsonLoader> createState() => _DlLottieJsonLoaderState();
}

class _DlLottieJsonLoaderState extends State<DlLottieJsonLoader> with TickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: (5)),
      vsync: this,
    );
  }

  @override
  dispose() {
    _controller.dispose(); // you need this
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return cupertinoSplash(context);
  }

  CupertinoPageScaffold cupertinoSplash(BuildContext context) {
    return CupertinoPageScaffold(
      child: lottieAnimation(context),
    );
  }

  Scaffold materialSplash(BuildContext context) {
    return Scaffold(
      body: lottieAnimation(context),
    );
  }

  LottieBuilder lottieAnimation(BuildContext context) {
    var animationProvided = widget.assetPath != null || widget.lottieJson != null;
    assert(animationProvided, DlLottieJsonLoader.assertionFailureRequiresAssetFileOrJsonString);
    return widget.assetPath == null
        ? lottieAnimationFromJsonString(context)
        : lottieAnimationFromAsset(context);
  }

  LottieBuilder lottieAnimationFromAsset(BuildContext context) {
    return Lottie.asset(
      widget.assetPath ?? "",
      controller: _controller,
      height: MediaQuery.of(context).size.height * 1,
      animate: true,
      repeat: true,
      onLoaded: (composition) {
        _controller
          ..duration = composition.duration
          ..repeat();
      },
    );
  }

  LottieBuilder lottieAnimationFromJsonString(BuildContext context) {
    var lottieJsonObject = jsonDecode(widget.lottieJson ?? "");
    var lottieBytes = JsonUtf8Encoder().convert(lottieJsonObject);
    var lottieUInt8 = Uint8List.fromList(lottieBytes);

    return Lottie.memory(
      lottieUInt8,
      controller: _controller,
      height: MediaQuery.of(context).size.height * 1,
      animate: true,
      repeat: true,
      onLoaded: (composition) {
        _controller
          ..duration = composition.duration
          ..repeat();
      },
    );
  }
}